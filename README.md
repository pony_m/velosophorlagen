# Xerosophen Flows

Ein Repository für die Erstellung von PDF aus Bestellungen. Enthält:

- Vorlagen für Briefe/PDF's (Vorlagen-Ordner)

- Allfällige Logos, die für die Erstellung benötigt werden (Idealerweise **svg**)

- **Keine Excel** Files, da diese nicht normale Textfiles sind

Für nicht Coder, also in das geclonte Verzeichnis keine Dateien legen.

# Anleitung

Dieses Repository auf den Computer kopieren mit dem Tool **git**. 

```shell
git clone https://gitlab.com/pony_m/xerosophen.git
```

## Installation des cli tools

Das Tool für die Datenverarbeitung ist mit der Programmiersprache `python` geschrieben. Diese ist in der Regel installiert. 

Öffne eine neues Terminal:

- Windows: Suche nach *Command Prompt* (oder auf Deutsch irgendwie anders)

- Bei MacOs findest du es unter *Terminal*

Checke, ob `python` istalliert ist, wenn nicht, suche im Internet die Lösung.

```shell
python3 --version
```

Installiere das Tool für den lokalen Nutzer only:

```shell
pip3 install --user git+https://gitlab.com/pony_m/flows-cli.git
flows --version
# Hilfe für der Subcommands
flows actions --help
# Teste die PDF Generierung
flows pdf-report --template Vorlagen/Emailversand/xerosoph.yml test.pdf

# Action chain anstossen für den ersten Schritt, zum testen
flows-cli actions -p actions/csv_to_pdf.yml -v --ix 1
```

## Zweck des Tool

**Flows** versucht zu helfen, die Versandabläufe zu vereinfachen. Es verfügt über einen Baukasten an Aktionen, die hintereinander ausgeführt werden. Man führt es über die Kommandozeile aus, während dem man die Files Aktionen in Dateien editieren kann.

Vorlagen für Abläufe sowie Templates und andere Dateien können in Ordnerstrukturen strukturiert werden.

## Updates

Bekommt das Tool **flows** eine neue Version, kann es sein, dass die Vorlagen etwas angepasst werden müssen. Ich übernehmen das.

Die Anwender des CLI sollten sicherstellen, dass vor der Erstellung von PDF's:

1. Das Excel der Bestellungen aktuell ist (sofern mehrere Personen damit arbeiten, über Dropbox oder so synchronisiert).

2. Flows aktuell ist. Dies wird (später) über eine Statusnachricht beim Aufruf von **Flows** angezeigt.
   
   `pip install --user --upgrade git+https://gitlab.com/pony_m/flows-cli.git`

## Anwendung Verteilung von Bestellungen

Im konkreten Fall kann man das am besten anhand der Konfiguration für eine Kette von Arbeitsschritten zum Erstellen und Verteilen von Bestellungen illustrieren:

Es gibt folgende Datenquellen für das Tool:

- Ein Excel-File mit den Bestellungen

- Eine Vorlage für das PDF, das für jede Bestellung erstellt wird

- Eine Datei, die die Zuordnung von Postleitzahlen zu Lokalgruppen enthält (`gruppen.yml`)

Der Ablauf ist in `actions/csv_to_pdf.yml` illustriert:

1. Lies ein Excel ein (`read_as: csv`)

2. Filtere die Einträge auf der Spalte `status`. Gleichzeitig extrahiere die in `column_mapping` definierten Werte unter dem neuen Namen. Gruppiere alle Zeilen des CSV nach PLZ (und benutze dazu die key_map, die die PLZ wieder Gruppen zuordnet).

3. Speichere (`save_to`) das PDF unter `../generierte_pdfs/{{ group_by_group }}/*.pdf` wobei `group_by_group` dann der Gruppenname ist, der in `gruppen-yml`drinsteht für die PLZ. Sofern es dort drin keinen gibt, ist es die PLZ selbst.

## Anmerkung zu Schriften

- Die Schrift, die die Xerospophen verwendet ist eine Courier Schrift, die aber von Microsoft lizenziert ist und für die PDF-Generierung nicht zur Verfügung steht.

- Um nicht von den Schriftarten der einzelnen Computer abhängig zu sein, ist hier die TTF-Schrift *Courier Prime* schon im **flows** Programm mit dabei. Diese kommt der ursprünglichen recht nahe und ich habe es damit getestet.
