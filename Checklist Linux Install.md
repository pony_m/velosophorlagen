# Checklist Linux Install



1. Alter Laptop?

2. Grafikkarte drin? Wenn ja Nvidia oder AMD?



## Install

- Verschlüsselte Festplatte

- Boot vertgrössern manuell?

- neusten Kernel wählen 5.8 sofern vorhanden

- Bei Mint Kernel über das Tool wählen, sonst bei start in grub, evtl. grub config anpassen



## Programme (Desktop abhängig)

- Backup: TimeShift

- Festplatten formatieren: gparted

- Office: LibreOffice (preinstalled)

- Video: VLC

- Bootable USB erstellen: Balena Edger

- Signal Desktop und Telegram Apps (AppImage?)

- Nextcloud?

- Dropbox (shop?)

- Google Drive (shop?)

- Mail: Thunderbird

- FlowChartMaker: DrawIO

- Markdown Editor: MarkEd

- Illustrator-Ersatz: Inkscape (wirklich gutes Programm!)

- 



## Programmierung

- sudo apt install git
